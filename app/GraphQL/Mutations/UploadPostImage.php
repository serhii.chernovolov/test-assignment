<?php

namespace App\GraphQL\Mutations;

use App\Models\Post;

final class UploadPostImage
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function __invoke($_, array $args)
    {
        /** @var \Illuminate\Http\UploadedFile $file */
        $file = $args['file'];
        $path = $file->storePublicly('public/uploads');
        $post = Post::find($args['id']);
        $post->update(['image' => $path]);
        return $post;
    }
}
